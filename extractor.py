"""
Extracts data from a file and puts it into a matrix of feature vectors and
labels.
"""

import numpy as np

def extract(filename, feature_list=None, label_list=None, append_bias=False):
    """
    Reads data from a file and returns a numpy matrix of the data.

    Arguments:
        filename:       The name of the file containing the data. For now, must
                        be a comma-separated file (.csv).
        feature_list:   A list of all the possible feature values. If None,
                        implies that the features are continuous, numerical
                        values.
        label_list:     A list of all the possible labels. If None, implies that
                        the labels are continous, numerical values.
        append_bias:    Tells the extractor whether or not to append a constant
                        feature value of 1 to the end of the samples to account
                        for the bias parameter in linear models.
    Returns:
        An m x n numpy matrix where m is the number of samples in the file and
        n is the length of the feature vectors. (If append_bias is set to True,
        then the matrix will be m x (n+1) because of the added constant feature.)
    """
    # Check to see if the features are discrete.
    if (feature_list):
        # TODO: Implement the same extraction that was implemented for the ID3
        #       project. 
        pass

    # Check to see if the labels are discrete.
    if (label_list):
        # TODO: Implement the same extraction that was implemented for the ID3
        #       project. 
        pass
    
    # Create objects for the matrix of samples and the vector of labels.
    mat = None    
    labels = np.array([])

    # Read in the data from the file.
    lines = None
    with open(filename, 'r') as infile:
        lines = infile.readlines()

    # Process the data.
    first_line = True
    m = len(lines)  # The number of samples
    for i in xrange(len(lines)):
        parts = lines[i].strip().split(',')
        if label_list:
            pass
        else:
            labels = np.append(labels, float(parts[-1]))
        
        if first_line:
            # On our first pass through the data, set the matrix dimensions.
            n = 0
            if append_bias:
                n = len(parts)
            else:
                n = len(parts) - 1
            
            mat = np.zeros((m,n))
            first_line = False
        
        for j in xrange(len(parts) - 1):
            mat[i][j] = float(parts[j])
        if append_bias:
            mat[i][-1] = 1.0
        
    return mat, labels
